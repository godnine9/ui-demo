﻿namespace UIEffects
{
    /// <summary>
    /// Blur effect mode.
    /// </summary>
    public enum BlurMode
    {
        None = 0,
        FastBlur = 1,
        MediumBlur = 2,
        DetailBlur = 3,
    }

    /// <summary>
    /// Color effect mode.
    /// </summary>
    public enum ColorMode
    {
        Multiply = 0,
        Fill = 1,
        Add = 2,
        Subtract = 3,
    }

    /// <summary>
    /// Effect mode.
    /// </summary>
    public enum EffectMode
    {
        None = 0,
        Grayscale = 1,
        Sepia = 2,
        Nega = 3,
        Pixel = 4,
    }

    /// <summary>
    /// Shadow effect style.
    /// </summary>
    public enum ShadowStyle
    {
        None = 0,
        Shadow,
        Outline,
        Outline8,
        Shadow3,
    }
}
